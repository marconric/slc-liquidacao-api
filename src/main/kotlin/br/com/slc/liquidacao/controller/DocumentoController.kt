package br.com.slc.liquidacao.controller

import br.com.slc.liquidacao.model.response.ApiResponse
import br.com.slc.liquidacao.service.DocumentoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/v1")
@Api(description = "Documento", tags = ["Documento SLC0001"])
class DocumentoController(private val documentoService: DocumentoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @GetMapping("buscaDocId/{id}")
    @ApiOperation("Processo responsável por realizar busca de todos os Registros ")
    fun buscaDocId(@PathVariable id : Long): ApiResponse<Any> {
        log.info("Busca todos registros  Grupo SLC001 Liq Produto")
        return ApiResponse(documentoService.findById(id))
    }


}