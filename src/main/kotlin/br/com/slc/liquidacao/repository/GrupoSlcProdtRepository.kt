package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.GrupoSlcProdt
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@EnableJpaRepositories
interface GrupoSlcProdtRepository : JpaRepository<GrupoSlcProdt, Long>{

    fun findByCodProdtAndAndGrSlcLiqId(codProdt :String,grSlcLiqId:Long) : Optional<GrupoSlcProdt>
}