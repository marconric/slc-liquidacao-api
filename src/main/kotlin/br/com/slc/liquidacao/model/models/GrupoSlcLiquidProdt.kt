package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.model.enums.DebCredEnum
import br.com.slc.liquidacao.util.Util
import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.annotations.ApiModelProperty
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = GrupoSlcLiquidProdt.Companion.CONFIG.TABLE,schema = Util.Companion.CONFIG.SCHEMA)
data class GrupoSlcLiquidProdt(

    @Id
    @ApiModelProperty(value = "ID da tabela")
    @Column(name = "IDENTDLINHABILAT")
    var identdLinhaBilat: String? = null,

    @Column(name = "TpDeb_Cred")
    var tpDebCred: DebCredEnum? = null,

    @Column(name = "ISPBIFCredtd")
    var ispbIfCredTd: Long? = null,

    @Column(name = "ISPBIFDebtd")
    var ispbIfDebTd: Long? = null,

    @Column(name = "VlrLanc")
    var vlrLanc: Double = 0.0,

    @Column(name = "CNPJNLiqdantDebtd")
    var CNPJNLiqdantDebtd: String? = null,

    @Column(name = "NomCliDebtd")
    var NomCliDebtd: String? = null,

    @Column(name = "CNPJNLiqdantCredtd")
    var CNPJNLiqdantCredtd: String? = null,

    @Column(name = "NomCliCredtd")
    var NomCliCredtd: String? = null,

    @Column(name = "TpTranscSLC")
    var tpTranscSlc: String? = null,

    @Column(name = "prodId")
    var prodId: Long? = null,

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "prodId", referencedColumnName = "ID", insertable = false, updatable = false)
    private val grupoSlcProdt: GrupoSlcProdt? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "GRUPO_SLC_LIQUID_PRODT"
        }
    }
}