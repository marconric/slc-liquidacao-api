package br.com.slc.liquidacao

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class SlcLiquidacaoApiApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(SlcLiquidacaoApiApplication::class.java, *args)
        }
    }
}
