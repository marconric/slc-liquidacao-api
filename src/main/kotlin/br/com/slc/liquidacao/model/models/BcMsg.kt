package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = BcMsg.Companion.CONFIG.TABLE,schema = Util.Companion.CONFIG.SCHEMA)
data class BcMsg(

    @Id
    @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
    @Column(name = "ID")
    var id: Long? = null,

    @Column(name = "IdentdEmissor")
    var identdEmissor: Int? = null,

    @Column(name = "IdentdDestinatario")
    var identdDestinatario: Int? = null,

    @Column(name = "DomSist")
    var domSist: String? = null,

    @Column(name = "NUOp")
    var nuOp: String? = null,

    @Column(name = "SEQ_ID")
    val grupoSeqId: Long? = null,

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SEQ_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    val grupoSeq: GrupoSeq? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "BCMSG"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}