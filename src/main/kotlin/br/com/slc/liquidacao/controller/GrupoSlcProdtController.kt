package br.com.slc.liquidacao.controller

import br.com.slc.liquidacao.model.response.ApiResponse
import br.com.slc.liquidacao.service.GrupoSlcProdutoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/v1")
@Api(description = " GrupoSlcProdt", tags = ["Grupo SLC0001 Produto"])
class GrupoSlcProdtController(private val grupoSlcProdutoService: GrupoSlcProdutoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @GetMapping("findAllSlcProd")
    @ApiOperation("Processo responsável por realizar busca de todos os Registros ")
    fun findAllSlcProd(): ApiResponse<Any> {

        log.info("Busca todos registros  Grupo SLC001 Liq Produto")
        return ApiResponse(grupoSlcProdutoService.findAll())
    }


}