package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.GrupoSlcLiquid
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository

@Repository
@EnableJpaRepositories
interface GrupoSlcLiquidRepository : JpaRepository<GrupoSlcLiquid, String>