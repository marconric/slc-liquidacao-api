package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = GrupoSlcLiquid.Companion.CONFIG.TABLE, schema = Util.Companion.CONFIG.SCHEMA)
data class GrupoSlcLiquid(

    @Id
    @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
    @Column(name = "ID")
    var id: Long? = null,

    @Column(name = "DtLiquid")
    @JsonFormat(pattern = "yyyy-MM-dd")
    var dtLiquid: LocalDateTime? = null,

    @Column(name = "NumSeqCicloLiquid")
    var NumSeqCicloLiquid: Int? = null,

    @OneToMany(
        mappedBy = "grupoSlcLiquid",
        cascade = [CascadeType.PERSIST, CascadeType.REMOVE],
        fetch = FetchType.EAGER
    )
    @Fetch(value = FetchMode.SELECT)
    var grupoSlcProdt: List<GrupoSlcProdt?>? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "GRUPO_SLC_LIQUID"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}