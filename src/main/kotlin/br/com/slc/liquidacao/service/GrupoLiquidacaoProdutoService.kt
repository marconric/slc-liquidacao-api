package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.GrupoSlcLiquidProdt
import br.com.slc.liquidacao.repository.GrupoSlcLiquidProdtRepository
import br.com.slc.liquidacao.repository.queries.GrupoSlcLiquidProdtRepositoryQueries
import org.springframework.stereotype.Service
import java.util.*

@Service
class GrupoLiquidacaoProdutoService(val grupoSlcLiquidProdtRepository: GrupoSlcLiquidProdtRepository,
                                    private val grupoSlcLiquidProdtRepositoryQueries: GrupoSlcLiquidProdtRepositoryQueries) {

    fun saveOrUpdate(grupoSlcLiquidProdt: GrupoSlcLiquidProdt): GrupoSlcLiquidProdt? {
        val grupoLiqOptional = grupoSlcLiquidProdt.identdLinhaBilat?.let { grupoSlcLiquidProdtRepository.findByIdentdLinhaBilat(it) }
        return if (grupoLiqOptional?.isPresent == true) {
            grupoLiqOptional?.get()
        } else {
            grupoSlcLiquidProdtRepository.save(grupoSlcLiquidProdt)
        }
        return null
    }

    fun findAllGLP(limit: Int): List<GrupoSlcLiquidProdt> {
        return grupoSlcLiquidProdtRepositoryQueries.findAllGLP(limit)
    }

    fun findByIdentdLinhaBilat(identdLinhaBilat: String): Optional<GrupoSlcLiquidProdt> {
        return grupoSlcLiquidProdtRepository.findByIdentdLinhaBilat(identdLinhaBilat)
    }
}