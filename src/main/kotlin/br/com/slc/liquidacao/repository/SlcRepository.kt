package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.Slc
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@EnableJpaRepositories
interface SlcRepository : JpaRepository<Slc, Long>{

    fun findByCodMsgAndNumCtrlSlcAndAndIsPbif(codMsg: String,numCtrlSlc:String,isPbif: Number): Optional<Slc>
}