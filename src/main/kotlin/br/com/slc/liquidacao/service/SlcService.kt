package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.Slc
import br.com.slc.liquidacao.repository.SlcRepository
import org.springframework.stereotype.Service

@Service
class SlcService(private val slcRepository: SlcRepository) {

    fun saveOrUpdate(clsDoc: Slc): Slc {
        println(clsDoc)
        val slcOptional = clsDoc.codMsg?.let { clsDoc.numCtrlSlc?.let { it1 -> clsDoc.isPbif?.let { it2 -> slcRepository.findByCodMsgAndNumCtrlSlcAndAndIsPbif(it, it1, it2) } } }
        return if (slcOptional != null && slcOptional.isPresent) {
            slcOptional.get()
        } else {
            slcRepository.save(clsDoc)
        }
    }
}