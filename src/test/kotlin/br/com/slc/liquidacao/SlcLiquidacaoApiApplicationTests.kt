package br.com.slc.liquidacao

import br.com.slc.liquidacao.model.response.ApiResponse
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.io.BufferedReader
import java.io.FileReader
import java.net.URI


@SpringBootTest
@DataJpaTest
class SlcLiquidacaoApiApplicationTests {

    private val porta = "13000"
    private val versaoApi = "v1"
    private val urlBase = "http://localhost:$porta/api/$versaoApi/"

    @Autowired
    private val testTemplate = TestRestTemplate()

    @Test
    fun importaXml() {

        var ois: BufferedReader? = null
        //TODO(para testar ajustar file name do arquivo path absoluto da localização do arquivo que encontra no resource)

        ois = BufferedReader(FileReader("/home/ricardo/work_sicredi/slc-liquidacao-api/src/main/resources/arquivo/arquivo.xml"))
        var linha: String? = ""
        val xml = StringBuilder()

        while (ois.readLine().also { linha = it } != null) {
            xml.append(linha)
        }

        ois.close()

        val urlPautaInsert = "${urlBase}processar"
        val uri = URI(urlPautaInsert)


        val headers = HttpHeaders()
        headers.set("Content-Type","application/xml")

        val request: HttpEntity<String> = HttpEntity(xml.toString(), headers)

        val result: ResponseEntity<ApiResponse<*>>? = this.testTemplate.postForEntity(uri, request, ApiResponse::class.java)
        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
    }

}

