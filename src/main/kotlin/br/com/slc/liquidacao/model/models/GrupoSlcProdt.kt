package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import com.fasterxml.jackson.annotation.JsonIgnore
import io.swagger.annotations.ApiModelProperty
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = GrupoSlcProdt.Companion.CONFIG.TABLE,schema = Util.Companion.CONFIG.SCHEMA)
data class GrupoSlcProdt(

    @Id
    @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
    @Column(name = "ID")
    var id: Long? = null,

    @Column(name = "CodProdt")
    var codProdt: String? = null,

    @Column(name = "GR_SLC_LIQ_ID")
    var grSlcLiqId: Long? = null,

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GR_SLC_LIQ_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    var grupoSlcLiquid: GrupoSlcLiquid? = null,

    @OneToMany(
        mappedBy = "grupoSlcProdt",
        cascade = [CascadeType.PERSIST, CascadeType.REMOVE],
        fetch = FetchType.EAGER
    )
    @Fetch(value = FetchMode.SELECT)
    var grupoSlcLiquidProdt: List<GrupoSlcLiquidProdt?>? = null

) : Serializable {

    companion object {
        object CONFIG {
            const val TABLE = "GRUPO_SLC_PRODUTO"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}