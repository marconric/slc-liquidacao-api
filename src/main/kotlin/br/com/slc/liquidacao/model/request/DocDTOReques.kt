package br.com.slc.liquidacao.model.request

import br.com.slc.liquidacao.model.enums.DebCredEnum
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper


class Grupo_Seq {
    @JvmField
    var NumSeq = 0

    @JvmField
    var IndrCont: String? = null
}

class BCMSG {
    @JvmField
    var IdentdEmissor = 0

    @JvmField
    var IdentdDestinatario = 0

    @JvmField
    var Grupo_Seq: Grupo_Seq? = null

    @JvmField
    var DomSist: String? = null

    @JvmField
    var NUOp : String? = null
}

class Grupo_SLC0001_LiquidProdt {
    @JvmField
    var IdentdLinhaBilat: String? = null

    @JvmField
    var TpDeb_Cred: DebCredEnum? = null

    @JvmField
    var ISPBIFCredtd = 0L

    @JvmField
    var ISPBIFDebtd = 0L

    @JvmField
    var VlrLanc = 0.0

    @JvmField
    var CNPJNLiqdantDebtd: String? = null

    @JvmField
    var NomCliDebtd: String? = null

    @JvmField
    var CNPJNLiqdantCredtd: String? = null

    @JvmField
    var NomCliCredtd: String? = null

    @JvmField
    var TpTranscSLC : String? = null
}

class Grupo_SLC0001_Prodt {
    @JvmField
    @JacksonXmlElementWrapper(useWrapping = false)
    var Grupo_SLC0001_LiquidProdt: List<Grupo_SLC0001_LiquidProdt>? = null

    @JvmField
    var CodProdt: String? = null
}

class Grupo_SLC0001_Liquid {
    @JvmField
    var DtLiquid: String? = null

    @JvmField
    var NumSeqCicloLiquid = 0

    @JvmField
    @JacksonXmlElementWrapper(useWrapping = false)
    var Grupo_SLC0001_Prodt: List<Grupo_SLC0001_Prodt>? = null
}

class SLC0001 {
    @JvmField
    var CodMsg: String? = null

    @JvmField
    var NumCtrlSLC : String? = null

    @JvmField
    var ISPBIF = 0

    @JvmField
    var TpInf: String? = null

    @JvmField
    var Grupo_SLC0001_Liquid: Grupo_SLC0001_Liquid? = null

    @JvmField
    var DtHrSLC: String? = null

    @JvmField
    var DtMovto: String? = null
}

class SISMSG {
    @JvmField
    var SLC0001: SLC0001? = null
}

class DOC {
    @JvmField
    var BCMSG: BCMSG? = null

    @JvmField
    var SISMSG: SISMSG? = null
}


