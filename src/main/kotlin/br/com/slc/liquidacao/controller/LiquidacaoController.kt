package br.com.slc.liquidacao.controller

import br.com.slc.liquidacao.model.response.ApiResponse
import br.com.slc.liquidacao.model.request.DOC
import br.com.slc.liquidacao.service.LiquidacaoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/v1")
@Api(description = "Liquidação", tags = ["Liquidação Unilateral"])
class LiquidacaoController(private val liquidacaoService: LiquidacaoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    @PostMapping(
        "processar",
        consumes = ["application/xml"],
        produces = ["application/xml"]
    )
    @ApiOperation("Processo efetua inserção de liquidação by XML")
    fun post(@RequestBody docRequest: DOC): ApiResponse<Any> {
        log.info("Inicio chamada Insert")

        return ApiResponse(liquidacaoService.incluirLancamento(docRequest))
    }


}