package br.com.slc.liquidacao.controller

import br.com.slc.liquidacao.model.response.ApiResponse
import br.com.slc.liquidacao.service.GrupoLiquidacaoProdutoService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/v1")
@Api(description = " GrupoSlcLiquidProdt", tags = ["Grupo SLC0001 Liquida Produto"])
class GrupoSlcLiquidProdtController(private val grupoLiquidacaoProdutoService: GrupoLiquidacaoProdutoService) {

    private var log = LoggerFactory.getLogger(this::class.java)

    private val limit: Int = 100

    @GetMapping("findAllGLP")
    @ApiOperation("Processo responsável por realizar busca de todos os Registros ")
    fun findAllGLP(): ApiResponse<Any> {
        //Todo(Este método criado só para exercício baseado nos arquivos deve se pensar uma estratégia de paginação)
        log.info("Busca todos registros  Grupo SLC001 Liq Produto")
        return ApiResponse(grupoLiquidacaoProdutoService.findAllGLP(limit))
    }


    @GetMapping("findGLPByIdentificador/{identdLinhaBilat}")
    @ApiOperation("Busca registros por identificador Linha Bilateral")
    fun findByIdentdLinhaBilat(@RequestParam identdLinhaBilat: String): ApiResponse<Any> {
        log.info("Busca registro Grupo SLC001 Liq Produto por Indentificador unilateral $identdLinhaBilat")
        val retorno = grupoLiquidacaoProdutoService.findByIdentdLinhaBilat(identdLinhaBilat)
        return if (retorno.isPresent) {
            ApiResponse(retorno.get())
        } else {
            return ApiResponse("Registro não encontrado")
        }

    }

}