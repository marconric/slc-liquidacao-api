package br.com.slc.liquidacao.repository.impl

import br.com.slc.liquidacao.model.models.GrupoSlcLiquidProdt
import br.com.slc.liquidacao.repository.queries.GrupoSlcLiquidProdtRepositoryQueries
import org.springframework.stereotype.Service
import javax.persistence.EntityManager


@Service
class GrupoSlcLiquidProdtRepositoryImpl(private val em: EntityManager) : GrupoSlcLiquidProdtRepositoryQueries {

    override fun findAllGLP(limit: Int): List<GrupoSlcLiquidProdt> {
        return em.createQuery("SELECT p FROM GrupoSlcLiquidProdt p ORDER BY p.identdLinhaBilat ",
                GrupoSlcLiquidProdt::class.java).setMaxResults(limit).getResultList()
    }


}