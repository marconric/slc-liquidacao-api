# slc-liquidacao-api

##Descrição
slc-liquidacao-api é uma API para realizar Serviço de Liquidação Centralizada Unilateral via arquivo XML
Este exercício tratando-se de uma maneira de verificação dos conhecimento técticos e boas práticas no desenvolvimento de Back-End
---

##Tecnologias envolvidas
Java 8/ Kotlin / Hibernate  / Spring /  H2  / GIT

---

##Processo para rodar API
 Verificar se o servidor possui banco H2 rodando se caso não possuir segue link para realização da configuração do mesmo
[Instalação H2](https://o7planning.org/11895/install-h2-database-and-use-h2-console)

####Configurações a serem realizadas (application)

Após realizar a configuração do banco no passo anterior e criar diretório para armazenar 

**Verificar as configurações da base que foi configurado na base H2 para ajustar na API**

> url: Caminho Base   EX. (jdbc:h2:/home/user/db/base;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE)
> 
> username: Usuário
> 
> password: Senha

Neste existem 3 arquivos de configuração YML
**application.yml** - arquivo do projeto aonde consta o profile que será usado para configurar o ambiente

**application-dev.yml** - Apontamento para utilização em ambiente de desenvolvimento

**appication-container.yml** Apontamento para utilização em ambiente de produção 

[Aplication YML](src/main/resources/application.yml) 

####Configurações a serem realizadas
> SLC_PORTA  Porta que vai rodar a aplicação está setado a 13000 como default
> 
> SLC_BASE Cuidado com esta configuração para rodar a primeira vez e criar os objetos da base nesta antes de rodar a aplicação e com a base H2 já configurada setar (create) após rodar a primeira vez alterar para (update ou none)   

 ---



####Modelo documento consumido pelo serviço POST (processar)
Serviços Rest POST para processar arquivo XML que segue modelo

```xml
<?xml version="1.0" encoding="UTF-8"?>
<DOC xmlns="http://www.bcb.gov.br/SPB/SLC0001.xsd">
   <BCMSG>
      <IdentdEmissor>99999999</IdentdEmissor>
      <IdentdDestinatario>12345678</IdentdDestinatario>
      <Grupo_Seq>
         <NumSeq>1</NumSeq>
         <IndrCont>N</IndrCont>
      </Grupo_Seq>
      <DomSist>SPB01</DomSist>
      <NUOp>12345678912345678912345</NUOp>
   </BCMSG>
   <SISMSG>
      <SLC0001>
         <CodMsg>SLC0001</CodMsg>
         <NumCtrlSLC>12345678912345678912</NumCtrlSLC>
         <ISPBIF>12345678</ISPBIF>
         <TpInf>P</TpInf>
         <Grupo_SLC0001_Liquid>
            <DtLiquid>2019-01-01</DtLiquid>
            <NumSeqCicloLiquid>1</NumSeqCicloLiquid>
            <Grupo_SLC0001_Prodt>
               <CodProdt>ACC</CodProdt>
               <Grupo_SLC0001_LiquidProdt>
                  <IdentdLinhaBilat>12345678ACC3456789</IdentdLinhaBilat>
                  <TpDeb_Cred>D</TpDeb_Cred>
                  <ISPBIFCredtd>99999999</ISPBIFCredtd>
                  <ISPBIFDebtd>99999999</ISPBIFDebtd>
                  <VlrLanc>4000.00</VlrLanc>
                  <CNPJNLiqdantDebtd>99929009000149</CNPJNLiqdantDebtd>
                  <NomCliDebtd>JET</NomCliDebtd>
                  <CNPJNLiqdantCredtd>17061147000194</CNPJNLiqdantCredtd>
                  <NomCliCredtd>BANCO</NomCliCredtd>
                  <TpTranscSLC>02</TpTranscSLC>
               </Grupo_SLC0001_LiquidProdt>
            </Grupo_SLC0001_Prodt>
            <Grupo_SLC0001_Prodt>
               <CodProdt>VCC</CodProdt>
               <Grupo_SLC0001_LiquidProdt>
                  <IdentdLinhaBilat>12345678VCC3456789</IdentdLinhaBilat>
                  <TpDeb_Cred>C</TpDeb_Cred>
                  <ISPBIFCredtd>99999999</ISPBIFCredtd>
                  <ISPBIFDebtd>99999999</ISPBIFDebtd>
                  <VlrLanc>16990.85</VlrLanc>
                  <CNPJNLiqdantDebtd>47515515000198</CNPJNLiqdantDebtd>
                  <NomCliDebtd>BANCOBOB</NomCliDebtd>
                  <CNPJNLiqdantCredtd>02181332000145</CNPJNLiqdantCredtd>
                  <NomCliCredtd>BANCO</NomCliCredtd>
                  <TpTranscSLC>02</TpTranscSLC>
               </Grupo_SLC0001_LiquidProdt>
               <Grupo_SLC0001_LiquidProdt>
                  <IdentdLinhaBilat>10045678VCC3456789</IdentdLinhaBilat>
                  <TpDeb_Cred>C</TpDeb_Cred>
                  <ISPBIFCredtd>99999999</ISPBIFCredtd>
                  <ISPBIFDebtd>99999999</ISPBIFDebtd>
                  <VlrLanc>3171.76</VlrLanc>
                  <CNPJNLiqdantDebtd>50276286000100</CNPJNLiqdantDebtd>
                  <NomCliDebtd>TRIANGULO</NomCliDebtd>
                  <CNPJNLiqdantCredtd>23333472000161</CNPJNLiqdantCredtd>
                  <NomCliCredtd>BANCO</NomCliCredtd>
                  <TpTranscSLC>02</TpTranscSLC>
               </Grupo_SLC0001_LiquidProdt>
            </Grupo_SLC0001_Prodt>
            <Grupo_SLC0001_Prodt>
               <CodProdt>MCC</CodProdt>
               <Grupo_SLC0001_LiquidProdt>
                  <IdentdLinhaBilat>12345678MCC1633000</IdentdLinhaBilat>
                  <TpDeb_Cred>C</TpDeb_Cred>
                  <ISPBIFCredtd>99999999</ISPBIFCredtd>
                  <ISPBIFDebtd>99999999</ISPBIFDebtd>
                  <VlrLanc>5462.12</VlrLanc>
                  <CNPJNLiqdantDebtd>93052785000127</CNPJNLiqdantDebtd>
                  <NomCliDebtd>TRIANGULOBOB</NomCliDebtd>
                  <CNPJNLiqdantCredtd>07284171000139</CNPJNLiqdantCredtd>
                  <NomCliCredtd>BANCO</NomCliCredtd>
                  <TpTranscSLC>02</TpTranscSLC>
               </Grupo_SLC0001_LiquidProdt>
            </Grupo_SLC0001_Prodt>
            <Grupo_SLC0001_Prodt>
               <CodProdt>CBC</CodProdt>
               <Grupo_SLC0001_LiquidProdt>
                  <IdentdLinhaBilat>12345678CBC1633012</IdentdLinhaBilat>
                  <TpDeb_Cred>C</TpDeb_Cred>
                  <ISPBIFCredtd>99999999</ISPBIFCredtd>
                  <ISPBIFDebtd>99999999</ISPBIFDebtd>
                  <VlrLanc>21446.69</VlrLanc>
                  <CNPJNLiqdantDebtd>74816392000106</CNPJNLiqdantDebtd>
                  <NomCliDebtd>BANCOBOB</NomCliDebtd>
                  <CNPJNLiqdantCredtd>30101382000100</CNPJNLiqdantCredtd>
                  <NomCliCredtd>TRIANGULOBANCO</NomCliCredtd>
                  <TpTranscSLC>02</TpTranscSLC>
               </Grupo_SLC0001_LiquidProdt>
            </Grupo_SLC0001_Prodt>
         </Grupo_SLC0001_Liquid>
         <DtHrSLC>2019-01-01T08:40:21</DtHrSLC>
         <DtMovto>2019-01-01</DtMovto>
      </SLC0001>
   </SISMSG>
</DOC>
```
####Fluxo do processamento 
```mermaid
graph TD 
A((Post - Processar))
 --> B{Valida Arquivo} 
 --> C(Processa Arquivo)
 --> D(Persiste Informação)
 --> E>Retorno Status]
  ```