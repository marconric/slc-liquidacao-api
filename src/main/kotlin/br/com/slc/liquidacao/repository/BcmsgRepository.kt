package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.BcMsg
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BcmsgRepository : JpaRepository<BcMsg, Long> {

    fun findByNuOpAndDomSist(nuOp: String, domSist: String): Optional<BcMsg>

}