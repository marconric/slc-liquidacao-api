package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.GrupoSlcProdt

import br.com.slc.liquidacao.repository.GrupoSlcProdtRepository
import org.springframework.stereotype.Service


@Service
class GrupoSlcProdutoService(val grupoSlcProdtRepository: GrupoSlcProdtRepository) {

    fun saveOrUpdate(grupoSlcProdt: GrupoSlcProdt): GrupoSlcProdt? {
        val grupoSLCProdutoOptional = grupoSlcProdt.codProdt?.let { grupoSlcProdt.grSlcLiqId?.let { it1 -> grupoSlcProdtRepository.findByCodProdtAndAndGrSlcLiqId(it, it1) } }
        return if (grupoSLCProdutoOptional?.isPresent == true) {
            grupoSLCProdutoOptional?.get()
        } else {
            return grupoSlcProdtRepository.save(grupoSlcProdt)
        }
    }


    fun findAll(): List<GrupoSlcProdt> {
        return grupoSlcProdtRepository.findAll()
    }

}