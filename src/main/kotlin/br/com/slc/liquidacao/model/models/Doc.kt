package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = Doc.Companion.CONFIG.TABLE, schema = Util.Companion.CONFIG.SCHEMA)
data class Doc(

        @Id
        @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
        @Column(name = "ID")
        var id: Long? = null,

        @Column(name = "BCMSG_ID")
        val bcmsgId: Long? = null,

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "BCMSG_ID", referencedColumnName = "ID", insertable = false, updatable = false)
        val Bcmsg: BcMsg? = null,

        @Column(name = "SLC0001_ID")
        val slc0001Id: Long? = null,

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "SLC0001_ID", referencedColumnName = "ID", insertable = false, updatable = false)
        val slc: Slc? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "DOC"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}