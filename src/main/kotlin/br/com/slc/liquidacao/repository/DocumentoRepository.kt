package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.Doc
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@EnableJpaRepositories
interface DocumentoRepository : JpaRepository<Doc, Long> {

    fun findBySlc0001IdAndBcmsgId(slc0001Id: Long, bcmsgId: Long): Optional<Doc>?
}