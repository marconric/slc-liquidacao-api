package br.com.slc.liquidacao.repository.queries
import br.com.slc.liquidacao.model.models.GrupoSlcLiquidProdt
import org.springframework.stereotype.Repository


@Repository
interface GrupoSlcLiquidProdtRepositoryQueries {

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    fun findAllGLP(imit: Int): List<GrupoSlcLiquidProdt>

}