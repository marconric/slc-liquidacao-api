package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = Slc.Companion.CONFIG.TABLE, schema = Util.Companion.CONFIG.SCHEMA)
data class Slc(

        @Id
        @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
        @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
        @Column(name = "ID")
        var id: Long? = null,

        @Column(name = "CodMsg")
        var codMsg: String? = null,

        @Column(name = "NumCtrlSLC")
        var numCtrlSlc: String? = null,

        @Column(name = "ISPBIF")
        var isPbif: Number? = null,

        @Column(name = "TpInf")
        var tpInf: String? = null,

        @Column(name = "DtMovto")
        @JsonFormat(pattern = "yyyy-MM-dd")
        var dtMovto: LocalDateTime? = null,

        @Column(name = "DtHrSLC")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Sao_Paulo")
        var dtHrSlc: LocalDateTime? = null,

        @Column(name = "GR_SL_ID")
        var grupoSlLiqId: Long? = null,

        @OneToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "GR_SL_ID", referencedColumnName = "ID", insertable = false, updatable = false)
        var grupoSlLiq: GrupoSlcLiquid? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "SLC0001"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}