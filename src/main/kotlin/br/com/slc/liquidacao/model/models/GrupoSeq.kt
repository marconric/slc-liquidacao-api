package br.com.slc.liquidacao.model.models

import br.com.slc.liquidacao.util.Util
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import javax.persistence.*

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = GrupoSeq.Companion.CONFIG.TABLE,schema = Util.Companion.CONFIG.SCHEMA)
data class GrupoSeq(

    @Id
    @SequenceGenerator(name = CONFIG.SEQUENCE, sequenceName = CONFIG.SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = CONFIG.SEQUENCE)
    @Column(name = "ID")
    var id: Long? = null,

    @Column(name = "NumSeq")
    var numSeq: Int? = 1,

    @Column(name = "IndrCont")
    var indrCont: String? = null

) : Serializable {
    companion object {
        object CONFIG {
            const val TABLE = "GRUPO_SEQ"
            const val SEQUENCE = "${TABLE}_SEQ_ID"
        }
    }
}