package br.com.slc.liquidacao.model.enums

enum class DebCredEnum(val tp: String, val descricao: String) {
    D("D", "Débito"),
    C("C", "Crédito");

    companion object {
        fun getById(id: String): DebCredEnum? = values().firstOrNull { it.tp == id }
    }
}