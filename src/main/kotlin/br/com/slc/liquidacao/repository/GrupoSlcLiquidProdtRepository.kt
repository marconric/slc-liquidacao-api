package br.com.slc.liquidacao.repository

import br.com.slc.liquidacao.model.models.GrupoSlcLiquidProdt

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@EnableJpaRepositories
interface GrupoSlcLiquidProdtRepository : JpaRepository<GrupoSlcLiquidProdt, String> {

    fun findByIdentdLinhaBilat(identdLinhaBilat: String): Optional<GrupoSlcLiquidProdt>
}