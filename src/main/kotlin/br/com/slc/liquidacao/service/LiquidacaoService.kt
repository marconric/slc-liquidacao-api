package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.*
import br.com.slc.liquidacao.model.request.DOC
import br.com.slc.liquidacao.repository.*
import br.com.slc.liquidacao.util.Util
import org.springframework.stereotype.Service

@Service
class LiquidacaoService(
        private val documentoService: DocumentoService,
        private val grupoSeqRepository: GrupoSeqRepository,
        private val bcmsgService: BcmsgService,
        private val slcService: SlcService,
        private val grupoSlcLiquidRepository: GrupoSlcLiquidRepository,
        private val grupoLiquidacaoProdutoService: GrupoLiquidacaoProdutoService,
        private val grupoSlcProdutoService: GrupoSlcProdutoService
) {

    fun incluirLancamento(objRequest: DOC): Any? {


        val bcmsgOptional = objRequest.BCMSG?.NUOp?.let { objRequest.BCMSG?.DomSist?.let { it1 -> bcmsgService.findByNuOpAndDomSist(it, it1) } }
        var bcmsg: BcMsg?

        if (bcmsgOptional != null && bcmsgOptional.isPresent) {
            bcmsg = bcmsgOptional.get()
        } else {
            val grSeq = grupoSeqRepository.save(
                    GrupoSeq(
                            null,
                            objRequest.BCMSG?.Grupo_Seq?.NumSeq,
                            objRequest.BCMSG?.Grupo_Seq?.IndrCont
                    )
            )

            bcmsg = bcmsgService.saveOrUpdate(
                    BcMsg(
                            null,
                            objRequest.BCMSG?.IdentdEmissor,
                            objRequest.BCMSG?.IdentdDestinatario,
                            objRequest.BCMSG?.DomSist,
                            objRequest.BCMSG?.NUOp,
                            grSeq.id
                    )
            )
        }

        val objGrupoLiq = objRequest?.SISMSG?.SLC0001?.Grupo_SLC0001_Liquid

        val grLiq = grupoSlcLiquidRepository.save(
                GrupoSlcLiquid(
                        null,
                        objGrupoLiq?.DtLiquid?.let { Util.stringFromDate(it) },
                        objGrupoLiq?.NumSeqCicloLiquid
                )
        )

        var listGrProd: ArrayList<GrupoSlcProdt> = arrayListOf()
        objGrupoLiq?.Grupo_SLC0001_Prodt?.forEach { grupo ->
            val grProd = grupoSlcProdutoService.saveOrUpdate(GrupoSlcProdt(null, grupo.CodProdt, grLiq.id))
            var listgrlp: ArrayList<GrupoSlcLiquidProdt> = arrayListOf()
            grupo.Grupo_SLC0001_LiquidProdt?.forEach { objGrlp ->

                val grlp = GrupoSlcLiquidProdt(
                        objGrlp.IdentdLinhaBilat,
                        objGrlp.TpDeb_Cred,
                        objGrlp.ISPBIFCredtd,
                        objGrlp.ISPBIFDebtd,
                        objGrlp.VlrLanc,
                        objGrlp.CNPJNLiqdantDebtd,
                        objGrlp.NomCliDebtd,
                        objGrlp.CNPJNLiqdantCredtd,
                        objGrlp.NomCliCredtd,
                        objGrlp.TpTranscSLC,
                        grProd?.id
                )

                grupoLiquidacaoProdutoService.saveOrUpdate(grlp)?.let { listgrlp.add(it) }
            }

            grProd?.grupoSlcLiquidProdt = listgrlp

            grupoSlcProdutoService.saveOrUpdate(grProd!!)?.let { listGrProd.add(it) }
        }
        grLiq.grupoSlcProdt = listGrProd

        grupoSlcLiquidRepository.save(grLiq)


        val objSlc = objRequest.SISMSG?.SLC0001
        val slc = slcService.saveOrUpdate(
                Slc(
                        null,
                        objSlc?.CodMsg,
                        objSlc?.NumCtrlSLC,
                        objSlc?.ISPBIF,
                        objSlc?.TpInf,
                        objSlc?.DtMovto?.let { Util.stringFromDate(it) },
                        objSlc?.DtHrSLC?.let { Util.stringFromDateHr(it) },
                        grLiq.id
                )
        )

        var doc = documentoService.saveOrUpdate(Doc(null, bcmsg?.id, null, slc.id, null))

        return doc.id
    }
}