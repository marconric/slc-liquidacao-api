package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.BcMsg
import br.com.slc.liquidacao.repository.BcmsgRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class BcmsgService(private val bcmsgRepository: BcmsgRepository) {

    fun saveOrUpdate(bcmsg: BcMsg): BcMsg? {
        return if (bcmsg.id != null) {
            bcmsg
        } else {
            bcmsgRepository.save(bcmsg)
        }
    }

    fun findByNuOpAndDomSist(nuOp: String, domSist: String): Optional<BcMsg> {
        //TODO(Vou persistir baseado no Numero Operação e domSis)
        return bcmsgRepository.findByNuOpAndDomSist(nuOp, domSist)
    }
}