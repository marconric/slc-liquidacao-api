package br.com.slc.liquidacao.service

import br.com.slc.liquidacao.model.models.Doc
import br.com.slc.liquidacao.repository.DocumentoRepository
import org.springframework.stereotype.Service

@Service
class DocumentoService(private val documentoRepository: DocumentoRepository) {

    fun findById(id: Long): Any? {
        val docOp = documentoRepository.findById(id)
        return if (docOp.isPresent) {
            docOp.get()
        } else {
            "Documento $id não Localizado"
        }
    }

    fun saveOrUpdate(doc: Doc): Doc {
        var docOptional = doc.slc0001Id?.let { documentoRepository.findBySlc0001IdAndBcmsgId(it, doc.slc0001Id) }
        return if (docOptional != null && docOptional.isPresent) {
            docOptional.get()
        } else {
            documentoRepository.save(doc)
        }
    }
}