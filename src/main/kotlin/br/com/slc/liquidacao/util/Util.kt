package br.com.slc.liquidacao.util

import java.io.Serializable
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Util {
    companion object {
        fun dataFormatada(data: Date, format: String? = "dd/MM/yyyy"): String {
            return SimpleDateFormat(format).format(data)
        }

        fun dataFormatada(data: LocalDateTime, format: String? = "dd/MM/yyyy HH:mm:ss"): String {
            return DateTimeFormatter.ofPattern(format).format(data)
        }

        fun dataFormatada(data: LocalDate, format: String? = "dd/MM/yyyy"): String {
            return DateTimeFormatter.ofPattern(format).format(data)
        }


        fun stringFromDateHr(str: String): LocalDateTime {
            return LocalDateTime.parse(str)
        }

        fun stringFromDate(str: String, format: String? = "yyyy-MM-dd"): LocalDateTime {
            val formatter = DateTimeFormatter.ofPattern(format)
            return LocalDate.parse(str, formatter).atStartOfDay()
        }

        object CONFIG {
            const val SCHEMA = "PUBLIC"
        }

        fun serializableToLong(id: Serializable?): Long? {
            if (id != null) {
                if (id is String) {
                    return id.toLong()
                } else if (id is Number) {
                    return id.toLong()
                } else {
                    // println("nao esta certo")
                    return -1
                }
            }

            return null
        }
    }


}